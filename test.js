const Db = require('./index.js')

const tradeDb = new Db('trade_journal', {directory:'./data'})
const settingsDb = new Db('settings', {directory:'./data'})

const compose = require('crocks/helpers/compose')

const hasStr = str => x => new RegExp(str, 'i').test(x)

const log = msg => x =>
	(console.log(msg, x), x)

const getRecord = name => db =>
	db.record(name)

const update = (path, value, opts) => record =>
	record.update(path, value, opts)

const getCollection = name => db =>
	db.collection(name)

const getSettings = getRecord('settings')
const getTrades = getCollection('trades')

const remove = path => record =>
	record.remove(path)

const write = db => db.write()

const insert1000 = trades => {
	Array(1000).fill(0)
		.map((_, i) => {
			trades.insert({ reason: `Number ${i}` })
		})
	return trades
}

const insert1000ToTrades =
	compose(insert1000, getTrades)

console.time('tradeDB write')
console.time('settingsDB write')
tradeDb.connect()
	.map(insert1000ToTrades)
	.chain(write)
	.fork(
		log('Connection rejected'),
		() => {
			console.log('trades written.')
			console.timeEnd('tradeDB write')
		}
	)

const updateSettings =
	compose(
		update('system.settings.thing', 'mything', {upsert:true}),
		update('risk.goalPercent', 10, {upsert:true}),
		getSettings
	)

settingsDb.connect()
	.map(updateSettings)
	.chain(write)
	.fork(
		log('Connection rejected'),
		() => {
			console.log('Settings updated')
			console.timeEnd('settingsDB write')
		}
	)
/*

const getCollection = name => db =>
	db.collection(name)

const getTrades = getCollection('trades')

const getAnyZero = trades =>
	trades.find({reason:hasStr('0')})

const getZeroes =
	compose(getAnyZero, getTrades)

console.time('tradeDB query')
tradeDb.connect()
	.map(getZeroes)
	.fork(
		log('Rejected'),
		result => {
			console.timeEnd('tradeDB query')
			console.log('query result', result)
		})
*/

const Async = require('crocks/Async')
const fs = require('fs')
const { constant } = require('crocks/combinators')
const { compose } = require('crocks/helpers')
const { map } = require('crocks/pointfree')
const { Reject, Resolve, fromPromise, fromNode } = Async

const readFile = fromPromise(fs.promises.readFile)
const writeFile = fromPromise(fs.promises.writeFile)

const DATA_DIR = './data'

class Collection {
	constructor(db, name, dataset=[], debug) {
		db.data[name] = dataset
		this.db = db
		this.name = name
		this.dataset = dataset
		this.debug = debug
	}
	drop() {
		this.db.data[this.name] = undefined
		return this.db
	}
	find(query) {
		let results = this.dataset
			.filter(doc => {
				let keys = Object.keys(query)
				let matches = keys
					.filter(key => query[key](doc[key]))
				return matches.length === keys.length
			})
		if(this.debug) console.log(`${results.length} item(s) were found successfully.`)
		return results
	}
	update(query, update) {
		let updates = 0
		this.find(query)
			.map(doc => {
				updates++
				return Object.keys(update)
					.reduce((acc, key) => {
						acc[key] = update[key]
						return acc
					}, doc)
			})
		if(this.debug) console.log(`${updates} item(s) were updated successfully.`)
		return this
	}
	insert(value, options={}) {
		if(options.uniqueOnly) {
			if(this.dataset.map(JSON.stringify).includes(JSON.stringify(value))) {
				if(this.debug) console.log(`Could not insert value, value already exists.`)
			} else {
				this.dataset.push({ ...value })
			}
		} else if(typeof options.condition === 'function') {
			if(options.condition(this.dataset, value)) {
				this.dataset.push({ ...value })
			} else {
				if(this.debug) console.log(`Could not insert value, condition not met.`)
			}
		} else {
			this.dataset.push({ ...value })
		}
		if(this.debug) console.log('1 item inserted successfully.')
		return this
	}
	remove(query) {
		let origLength = this.dataset.length
		this.dataset = this.dataset
			.filter(doc => {
				let keys = Object.keys(query)
				let matches = keys.filter(key => (key in doc) && query[key](doc[key]))
				return !(matches.length && (matches.length === keys.length))
			})
		if(this.debug) console.log(`${origLength - this.dataset.length} item(s) were removed successfully.`, query)
		this.db.data[this.name] = this.dataset
		return this
	}
	write() {
		return this.db.write()
	}
	toArray() {
		return this.dataset.slice(0)
	}
}

class Record {
	constructor(db, name, dataset={}, debug) {
		db.data[name] = dataset
		this.db = db
		this.name = name
		this.dataset = dataset
		this.debug = debug
	}
	drop() {
		this.db.data[this.name] = undefined
		return db
	}
	find(keys) {
		let result = Resolve(
			keys.split('.')
				.reduce((acc, key) => acc ? acc[key] : undefined, this.dataset)
		)
		if(this.debug) console.log(`${result ? 1 : 0} items found successfully.`)
		return result
	}
	update(keys, value, options={}) {
		let path = keys.split('.')
		if(path.length > 1) {
			let last = path.pop()
			let notFound = false
			let storagePoint = path
				.reduce((acc, key) => {
					if(acc[key]) {
						return acc[key]
					} else {
						if(options.upsert) {
							acc[key] = {}
							return acc[key]
						} else {
							notFound = true
							return {}
						}
					}
				}, this.dataset)
			storagePoint[last] = value
			console.log('dataset', this.db.data[this.name])
			if(this.debug) console.log(`${Number(!notFound)} item updated successfully.`, keys, value)
		} else {
			this.dataset[keys] = value
			if(this.debug) console.log(`1 item updated successfully.`, keys, value)
		}
		return this
	}
	insert(keys, value) {
		let path = keys.split('.')
		if(path.length > 1) {
			let last = path.pop()
			let storagePoint = path
				.reduce((acc, key) => {
					acc[key] = {}
					return acc[key]
				}, this.dataset)
			storagePoint[last] = value
		} else {
			this.dataset[keys] = value
		}
		if(this.debug) {
			if(value === undefined) {
				console.log(`1 item removed successfully from ${keys}.`)
			} else {
				console.log(`1 item inserted successfully at ${keys}.`)
			}
		}
		return this
	}
	insertRecord(record) {
		this.db.data[this.name] = record
		return this
	}
	remove(keys) {
		return this.insert(keys, undefined)
	}
	write() {
		return this.db.write()
	}
	toObject() {
		return {...this.dataset}
	}
}

class Db {
	constructor(name='flattered', options={directory:DATA_DIR}) {
		options.directory = options.directory.replace(/\/$/, '')
		if(!fs.existsSync(options.directory)) fs.mkdirSync(options.directory)
		this.name = name
		this.options = options
		this.filepath = `${options.directory}/${name}.db.json`
	}
	connect() {
		let self = this
		if(self.data) {
			return Resolve(self)
		} else {
			return readFile(self.filepath, 'utf8')
				.map(JSON.parse)
				.coalesce(
					e => {
						self.data = {}
						return self
					},
					data => {
						self.data = data
						return self
					}
				)
		}
	}
	collection(name) {
		return new Collection(this, name, this.data[name], this.options.debug)
	}
	record(name) {
		return new Record(this, name, this.data[name], this.options.debug)
	}
	dropDatabase() {
		let self = this
		if(self.options.debug) console.log(`Dropping database ${self.name}`)
		self.data = {}
		return writeFile(self.filepath, JSON.stringify(self.data), {encoding:'utf8'})
			.bimap(constant(self), constant(self))
	}
	// write requires chain be used as it returns Async
	write() {
		let self = this
		if(self.options.debug) console.log(`Writing database ${self.name}`)
		if(Object.keys(self.data).length) {
			return writeFile(
				self.filepath,
				self.options.debug ? JSON.stringify(self.data, null, 2) : JSON.stringify(self.data),
				{ encoding: 'utf8' }
			)
			.coalesce(constant(self), constant(self))
		} else {
			return self.connect()
				.map(db => self)
		}
	}
}

module.exports = Db
